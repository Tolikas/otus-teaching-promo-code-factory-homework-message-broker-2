﻿using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Service;
using Otus.Teaching.Pcf.Common;

namespace Otus.Teaching.Pcf.Administration.WebHost.Bus
{
    public class AdministrationConsumer : IConsumer<AdministrationMessage>
    {
        private readonly IUpdateAppliedPromocodeService _updateAppliedPromocodeService;
        //private readonly ILogger<GivingToCustomerConsumer> _logger;

        public AdministrationConsumer(IUpdateAppliedPromocodeService updateAppliedPromocodeService, ILogger<AdministrationConsumer> logger)
        {
            _updateAppliedPromocodeService = updateAppliedPromocodeService;
            //_logger = logger;
        }

        public async Task Consume(ConsumeContext<AdministrationMessage> context)
        {
            await _updateAppliedPromocodeService.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId);
        }
    }
}