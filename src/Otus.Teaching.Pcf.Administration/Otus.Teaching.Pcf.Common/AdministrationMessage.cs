﻿using System;

namespace Otus.Teaching.Pcf.Common
{
    public class AdministrationMessage
    {
        public Guid PartnerManagerId { get; set; }
    }
}