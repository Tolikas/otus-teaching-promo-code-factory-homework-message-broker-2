﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Service
{
    public interface IUpdateAppliedPromocodeService
    {
        Task<bool> UpdateAppliedPromocodesAsync(Guid id);
    }
}