﻿using System;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;

namespace Otus.Teaching.Pcf.Administration.Core.Service
{
    public class UpdateAppliedPromocodeService : IUpdateAppliedPromocodeService
    {
        private readonly IRepository<Employee> _employeeRepository;

        public UpdateAppliedPromocodeService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task<bool> UpdateAppliedPromocodesAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
            {
                return await Task.FromResult(false);
            }

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);

            return await Task.FromResult(true);
        }
    }
}