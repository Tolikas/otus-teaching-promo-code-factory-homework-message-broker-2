﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.Common;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class AdministrationGatewayBus : IAdministrationGateway
    {
        private readonly IBus _bus;

        public AdministrationGatewayBus(IBus bus)
        {
            _bus = bus;
        }

        public async Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            var administrationMessage = new AdministrationMessage
            {
                PartnerManagerId = partnerManagerId
            };

            await _bus.Publish(administrationMessage);
        }
    }
}