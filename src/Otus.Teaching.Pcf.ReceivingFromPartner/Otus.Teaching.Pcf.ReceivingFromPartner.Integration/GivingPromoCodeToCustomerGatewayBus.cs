﻿using MassTransit;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Common;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class GivingPromoCodeToCustomerGatewayBus : IGivingPromoCodeToCustomerGateway
    {
        private readonly IBus _bus;

        public GivingPromoCodeToCustomerGatewayBus(IBus bus)
        {
            _bus = bus;
        }

        public async Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            var givingToCustomerMessage = new GivingToCustomerMessage
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo
            };

            await _bus.Publish(givingToCustomerMessage);
        }
    }
}