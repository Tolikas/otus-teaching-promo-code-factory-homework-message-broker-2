﻿using MassTransit;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Common;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Models;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Service;
//using Otus.Teaching.Pcf.Common.Message;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Bus
{
    public class GivingToCustomerConsumer : IConsumer<GivingToCustomerMessage>
    {
        private readonly IGivePromoCodeService _givePromoCodeService;
        //private readonly ILogger<GivingToCustomerConsumer> _logger;

        public GivingToCustomerConsumer(IGivePromoCodeService givePromoCodeService, ILogger<GivingToCustomerConsumer> logger)
        {
            _givePromoCodeService = givePromoCodeService;
            //_logger = logger;
        }

        public async Task Consume(ConsumeContext<GivingToCustomerMessage> context)
        {
            var request = new GivePromoCodeRequest
            {
                ServiceInfo = context.Message.ServiceInfo,
                PartnerId = context.Message.PartnerId,
                PromoCodeId = context.Message.PromoCodeId,
                PromoCode = context.Message.PromoCode,
                PreferenceId = context.Message.PreferenceId,
                BeginDate = context.Message.BeginDate,
                EndDate = context.Message.EndDate
            };

            await _givePromoCodeService.GivePromoCodesToCustomersWithPreferenceAsync(request);
        }
    }
}