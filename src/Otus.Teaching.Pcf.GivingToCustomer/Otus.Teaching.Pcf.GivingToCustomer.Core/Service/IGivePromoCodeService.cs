﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Models;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Service
{
    public interface IGivePromoCodeService
    {
        Task<bool> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request);
    }
}